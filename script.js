
//find users with letter "a" in their fname or lname
db.users.find(
	//query with logical operator OR
	{
		$or:[

			{"firstName":{$regex: "a", $options:"i"}},
			{"lastName":{$regex: "a", $options:"i"}}

		]
	},
	//projection, show only fname and lname, hide ID field
	{

		"firstName": 1,
		"lastName": 1,
		"_id": 0
	}
);

//find users who are admin and is active, explicitly used logical $and operator
db.users.find(

		{
			$and:[

				{"isAdmin": true},
				{"isActive": true}
			]
		}

);

//find courses with letter "u" in its name and has a price of greater than or equal to 13000, logical $and operator implicitly used
db.courses.find(

		{
			"name": {$regex: "u", $options: "i"},
			"price":{$gte: 13000}
		}

);

